# Nutsinee K. 6th March 2018
# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_SHG.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_SHG.py $

import sys
import time
from guardian import GuardState, GuardStateDecorator

nominal = 'LOCKED'
request = 'DOWN'

#############################################
#Functions

def SHG_locked():
    #this number will have to be adjusted as the input power changes
    #return ezca['SQZ-SHG_GR_DC_POWER']> 2
    return ezca['SQZ-SHG_TRANS_DC_NORMALIZED']>0.5
    #Maybe look at green reflected light instead. Make it dependent of input power. Check what's laser IR PD read when 100mW goes in.

def in_softfault():
    if ezca['SQZ-SHG_PZT_RANGE'] != 0:
        notify('PZT voltage limits exceeded.')
        return True
    elif not SHG_locked():
        notify('SHG not really locked or maybe locked in a wrong mode')
        return True


def in_hardfault():
    #if ezca['SQZ-LASER_IR_DC_ERROR_FLAG'] != 0:
    #    notify('Squeezer laser PD error')
    #    return True
    #elif ezca['SQZ-PMC_TRANS_DC_ERROR_FLAG'] != 0:
    #    notify('Squeezer PMC not locked')
    #    return True
    if ezca['SQZ-SHG_TRANS_RF24_PHASE_ERROR_FLAG'] != 0:
        notify('SHG Trans phase shifter error')
        return True
    #elif ezca['SQZ-SHG_TRANS_RF24_DEMOD_ERROR_FLAG'] != 0:
    #    notify('SHG Trans Demod error')
    # see comment to alog 83224 for why this was commented out
    #    return True


#############################################
#Decorator

class softfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_softfault():
            return 'SCANNING'

class hardfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_hardfault():
            return 'DOWN'


#############################################
#States

class INIT(GuardState):
    index = 0
    def main(self):
        if SHG_locked():
            return 'LOCKED'
        else:
            return 'IDLE'

class DOWN(GuardState):
    index = 1
    goto = True
    def main(self):
        ezca['SQZ-SHG_SERVO_IN1EN'] = 0
        ezca['SQZ-SHG_SERVO_SLOWOFSEN'] = 0
        ezca['SQZ-SHG_SERVO_SLOWOUTOFS'] = 0
        ezca['SQZ-SHG_SERVO_COMBOOST'] = 0
        ezca['SQZ-SHG_SERVO_SLOWBOOST']=1 #don't turn off compensation. Zero slow output with compensation on
        #ezca['SQZ-SHG_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-SHG_SERVO_IN1GAIN'] = -7 #was 7, vx changed 3/17 following xfer function measurements for ugh~280Hz #was -11, naoki changed 8/1 for ugf~1kHz #was 5, naoki changed on 2024/2/14 for 24MHz #was -8, naoki changed on 2024/2/20 after PMC installed
        ezca['SQZ-SHG_PZT_SCAN_ENABLE']=0 #turn off PZT scan

    def run(self):
        return True


class IDLE(GuardState):
    index = 3
    request = False

    def run(self):
        #SED does not find these notifactions helpful (not relevant to problems)
        #notify('Cannot lock SHG. Check SQZ laser power.')
        #log('Cannot lock SHG. Check SQZ laser power')
        #return ezca['SQZ-LASER_IR_DC_NORMALIZED'] > 0.05
        if ezca['SQZ-PMC_TRANS_DC_ERROR_FLAG'] == 0:
            return True
        else:
            notify('Waiting for squeezer PMC')



class SCANNING(GuardState):
    index = 4
    request = False

    def main(self):
        ezca['SQZ-SHG_SERVO_IN1EN']=0 #disengage SHG locking
        ezca['SQZ-SHG_SERVO_COMBOOST']=0 #disengage boosts
        #ezca['SQZ-SHG_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-SHG_SERVO_SLOWBOOST']=1 #this boost should always be on
        ezca['SQZ-SHG_PZT_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-SHG_PZT_SCAN_ENABLE']=1 #scan PZT

    @hardfault_checker
    def run(self):
        if ezca['SQZ-SHG_PZT_SCAN_ENABLE'] == 0:
            if ezca['SQZ-SHG_PZT_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level or laser power.')
                log('Scan timeout. Check trigger level or laser power.')
                return 'IDLE'
            return True
        return False

class LOCKING(GuardState):
    index = 5
    request = False

    def main(self):
        ezca['SQZ-SHG_SERVO_IN1EN'] = 1 #engage SHG locking
        time.sleep(0.3)

    @hardfault_checker
    def run(self):
        if ezca['SQZ-SHG_PZT_RANGE'] != 0: #if not okay
            notify('PZT out of range. Try again.')
            log('PZT out of range. Try again.')
            return 'IDLE'

        #check if SHG is locked. If so engage boost.
        if SHG_locked():
            ezca['SQZ-SHG_SERVO_COMBOOST'] = 1
            ezca['SQZ-SHG_SERVO_SLOWBOOST'] = 1
            ezca['SQZ-LO_SERVO_SLOWOPT'] = 1 #slow opt acts like a little boost. Used because Boost2 was too much
            return True
        else:
            return 'IDLE'

class POWER_STABILIZATION(GuardState):
    index = 9
    @softfault_checker
    @hardfault_checker
    def main(self):
        self.timer['pause'] = 0
    @softfault_checker
    @hardfault_checker
    def run(self):
        #if the power stabilization servo is railed, reset the offset
        if self.timer['pause']:
            if ezca['SQZ-SHG_PWR_AOM_LIMITERR']:
                ezca['SQZ-SHG_PWR_TRANS_OFFSET'] = -1* ezca['SQZ-OPO_TRANS_LF_OUT16']
                self.timer['pause'] = 1
            else:
                return True


class LOCKED(GuardState):
    index = 10

   # def main(self):
   #     ezca['SQZ-SHG_SERVO_IN1GAIN'] = 0

    @softfault_checker
    @hardfault_checker
    def run(self):
        if ezca['SQZ-SHG_PZT_VOLTS'] > 98:
            notify('SHG PZT volts high, may be noisy, fc may not lock')
        if ezca['SQZ-SHG_PZT_VOLTS'] < 5:
            notify('SHG PZT volts low, may be noisy, try relocking')
        return True




edges = [('INIT','DOWN'),
    ('INIT', 'LOCKED'),
    ('DOWN','IDLE'),
    ('IDLE','SCANNING'),
    ('SCANNING','LOCKING'),
    ('LOCKING','LOCKED'),
    ('LOCKING','POWER_STABILIZATION'),
    ('POWER_STABILIZATION', 'LOCKED')
]

